# QUICKSORT

## DESCRIPTION

Quicksort (sometimes called partition-exchange sort) is an efficient sorting algorithm. Developed by British computer scientist Tony Hoare in 1959 and published in 1961, it is still a commonly used algorithm for sorting.

Quicksort is a divide-and-conquer algorithm. It works by selecting a 'pivot' element from the array and partitioning the other elements into two sub-arrays, according to whether they are less than or greater than the pivot. The sub-arrays are then sorted **recursively**. This can be done **in-place**, requiring small additional amounts of memory to perform the sorting.

Mathematical analysis of quicksort shows that, on average, the algorithm takes **O(n log n)** comparisons to sort n items. In the worst case, it makes O(n2) comparisons, though this behavior is rare.
(Source [wikipedia])

## EXAMPLE

The generic steps are:
- choose the last element as pivot, loop_index starts at the first element (in the beggining 0), swap_index is start - 1 (in the beggining -1)
- loop from each of the elements until the end of the set (except the pivot) - loop_index is each step incremented by 1
- if the element > pivot do nothing
- if the element <= pivot do 1) increment swap_index by one 2) swap the element at swap_index with the current element (loop_index)

Once the looping is done, one final swap (put the pivot in the right position): the pivot with the swap_index + 1 element.
Then recursively sort the left, and the right set of elements.

Let's start with the initial array

    9   1   2   7   5   3   4

pivot = 4 loop_index = 0 swap_index = -1

Step 1:

Compare 9 <= 4 -> false, do nothing. loop_index = 1, swap_index = -1

    9   1   2   7   5   3   4

Step 2:

Compare 1 <= 4 -> true, swap_index = 0, swap set[0] with set[1] loop_index = 2

    1   9   2   7   5   3   4

Step 3:

Compare 2 <= 4 -> true, swap_index = 1, swap set[1] with set[2] loop_index = 3

    1   2   9   7   5   3   4

Step 4:

Compare 7 <= 4 -> false, do nothing. loop_index = 4, swap_index = 1

    1   2   9   7   5   3   4

Step 5:

Compare 5 <= 4 -> false, do nothing. loop_index = 5, swap_index = 1

    1   2   9   7   5   3   4

Step 6:

Compare 3 <= 4 -> true, swap_index = 2, swap set[2] with set[5] loop_index = 6

    1   2   3   7   5   9   4

This reached the end of the set.

Step 7:

Insert the pivot at the position swap_index + 1 (3)

    1   2   3   4   5   9   7

Now do the same (recursively) for [1, 2, 3] and  [5, 9, 7].

[wikipedia]: <https://en.wikipedia.org/wiki/Quicksort>