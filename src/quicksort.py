# -*- coding: utf-8 -*-


class QuickSort:
    def __init__(self, input):
        self.input = list(map(int, input))

    def sort(self):
        self.__quicksort(self.input, 0, len(self.input) - 1)

        return self.input

    def __quicksort(self, sortable, start, end):
        """ this is the redundant function, dividing and conquering ;) """
        if start >= end:
            return

        p = self.__partition(sortable, start, end)

        self.__quicksort(sortable, start, p - 1)
        self.__quicksort(sortable, p + 1, end)

    def __partition(self, sortable, start, end):
        loop_index = start
        swap_index = start - 1

        while loop_index < end:
            if sortable[loop_index] <= sortable[end]:
                swap_index += 1
                sortable[loop_index], sortable[swap_index] = sortable[swap_index], sortable[loop_index]
            loop_index += 1

        sortable[swap_index + 1], sortable[end] = sortable[end], sortable[swap_index + 1]

        return swap_index + 1
