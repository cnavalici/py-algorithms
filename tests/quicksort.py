import unittest
import random

from src.quicksort import QuickSort


class MyTestCase(unittest.TestCase):
    REPEAT = 1000

    def test_with_unique_values(self):
        expected_data = [1, 3, 4, 7, 8, 11, 14, 20, 21]
        input_data = expected_data.copy()

        for _ in range(0, self.REPEAT):
            random.shuffle(input_data)

            sorter = QuickSort(input_data)
            result = sorter.sort()

            self.assertListEqual(expected_data, result)

    def test_with_one_value(self):
        sorter = QuickSort([3])
        result = sorter.sort()

        self.assertListEqual([3], result)

    def test_with_duplicated_values(self):
        expected_data = [2, 2, 4, 4, 4, 5, 6, 7, 7, 88, 88, 88, 88]
        input_data = expected_data.copy()

        for _ in range(0, self.REPEAT):
            random.shuffle(input_data)

            sorter = QuickSort(input_data)
            result = sorter.sort()

            self.assertListEqual(expected_data, result)


if __name__ == '__main__':
    unittest.main()
