#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from src.quicksort import QuickSort

if __name__ == '__main__':
    str_numbers = input("Enter numbers separated by space: ")
    input_numbers = str_numbers.split(" ")

    quick = QuickSort(input_numbers)
    result = quick.sort()

    print(result)